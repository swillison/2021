---
name: Bank of America
tier: silver
site_url: https://careers.bankofamerica.com/mycareer/en-us/job-detail/21045554/python-software-engineer-iii-new-york-new-york-united-states
logo: bank-of-america.png
twitter: ""
---
Quartz is a massive Python development initiative that is revolutionizing the
Global Markets business at Bank of America. Quartz is the primary platform for
pricing trades, managing positions, and computing risk exposure. Our technology
stack includes globally distributed object-oriented databases, Linux compute
farms, and advanced quant libraries. Thousands of developers are using the
highly-agile platform to deliver applications to thousands of end users.

We are an organization with a product-oriented mindset that prioritizes quality,
engineering and teamwork. We are looking for exceptional software engineers from
diverse backgrounds who can lead, or significantly contribute to, enterprise
technology delivery. A background in Big Data, Data Science, DevOps or
large-scale architecture is ideal.  FinTech experience is nice to have.

Find out more by reaching out to [Aneesh
Shukla](mailto:aneesh.shukla@bofa.com?subject=PyGotham%202021%20follow%20up) and
[apply
today](https://careers.bankofamerica.com/mycareer/en-us/job-detail/21045554/python-software-engineer-iii-new-york-new-york-united-states).
