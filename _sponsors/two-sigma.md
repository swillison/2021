---
name: Two Sigma
tier: gold
site_url: https://www.twosigma.com/careers
logo: two-sigma.jpg
twitter: ""
---
Two Sigma is a financial sciences company. We combine rigorous inquiry, data
analysis, and invention to solve the toughest challenges across financial
services. We use Open Source Software, and we’re passionate about giving back to
the programming community. We want to make sure that the OSS projects that we
and so many others rely on will still be functional far into the future.
Supporting OSS can take many forms: making substantial contributions to projects
we use regularly, open sourcing tools we’ve developed in house, and financially
sponsoring non-profits that contribute to a healthy open source ecosystem.
