---
name: Bloomberg
tier: gold
site_url: https://www.techatbloomberg.com/?utm_campaign=MKTG_2021TechConferences&utm_content=site-partner&utm_medium=cto&utm_source=site-partner
logo: bloomberg.png
twitter: TechAtBloomberg
---
Bloomberg is building the world's most trusted information network for financial
professionals. Our 6,500+ engineers are dedicated to advancing and building new
systems for the Bloomberg Terminal to solve complex, real-world problems.
​We trust our teams to choose the right technologies for the job, and, at
Bloomberg, the answer is often Python. We employ an active community of 2,000+
Python developers who have their hands in everything from financial analytics
and data science to contributing to open source technologies like Project
Jupyter. Bloomberg is proud to be a visionary sponsor of the Python Software
Foundation and host of many Python community events around the world.
