---
title: Frequently Asked Questions
---

## I want to volunteer.
Great! We’re always looking for people to be session chairs, to help with
registration, and to be general runners. Please sign up for one of the available
openings in the {% if site.data.event.volunteer_signup_url %}[volunteer signup
form]({{ site.data.event.volunteer_signup_url }}){% else %}volunteer signup form
(coming soon){% endif %}, and we'll reach out with more details as we get closer
to the event. Feel free to ping us at
[volunteers@pygotham.org](mailto:volunteers@pygotham.org) if you have any
questions.

## Will the talks be recorded?
We record all talks but don't release the video of any talk whose author has
declined the [recording release]({% link speaking/recording-release.md %}).
Links to the videos will be posted on the site as well as on
[PyVideo](https://pyvideo.org) after they are available.

{% if site.data.event.repository_url %}
## Your website is great, but I found a typo or otherwise missing information.
Thanks for pointing that out! Please submit a fix at
[{{ site.data.event.repository_url }}]({{ site.data.event.repository_url }}).
{% endif %}

## I have another question, and I solemnly swear that it is not answered on this page already.
We're happy to answer it for you. Email us at
[organizers@pygotham.org](mailto:organizers@pygotham.org).
