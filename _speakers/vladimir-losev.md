---
name: Vladimir Losev
talks:
- "How we dealt with poor code analysis support in our generator, metaclasses, and code generation heavy projects"
---
Vladimir Losev, senior software engineer at Toloka https://toloka.ai
