---
name: Mark Smith
talks:
- "Stupid Things I\u0027ve Done with Python"
---
My name is Mark Smith, although I'm often known as Judy online. I'm a
Developer Advocate for MongoDB. I love writing stupid Python code in an
attempt to really understand how Python works. When I'm not doing this,
you'll find me crocheting, building custom keyboards, or designing models
for 3D printing.
