---
name: Simon Willison
talks:
- "How to build, test and publish an open source Python library"
---
Simon is the creator of Datasette, an open source tool for exploring and
publishing data.

Datasette is based on Simon's experiences working as a data journalist at
the UK's Guardian newspaper.

Simon is also a co-creator of the Django web framework. He recently
completed the JSK Fellowship program at Stanford.

https://simonwillison.net/ - [@simonw](https://twitter.com/simonw)
