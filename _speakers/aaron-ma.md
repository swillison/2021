---
name: Aaron Ma
talks:
- "Machine Learning without a Ph.D"
---
Hello world, it's Aaron! I'm proficient in C++ and Python. I 💖 robotics,
machine learning, algorithms, and competitive programming. I am also proud
to be the youngest TensorFlow and Baidu Apollo Auto contributor. Currently,
I'm an algorithms researcher working on studying and designing more
efficient algorithms. Don't forget to check out my 100% satisfaction
guaranteed website here: https://aaronhma.com/
