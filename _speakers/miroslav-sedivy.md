---
name: Miroslav Šedivý
talks:
- "There Are Python 2 Relics in Your Code"
---
Using Python to make the sun shine, the wind blow, and the gas flow. Greedy
polyglot, sustainable urbanist, unicode collector, wandering
openstreetmapper, and an hjkl juggler.
