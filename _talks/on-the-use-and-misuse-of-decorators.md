---
duration: 25
presentation_url:
room: Online
slot: 2021-10-01 12:20:00-04:00
speakers:
- Christopher Neugebauer
title: "On the Use and Misuse of Decorators"
type: talk
video_url:
---
One of the most fraught questions in Python is “What is a decorator?”. Most
answers are simple, easy to understand, and wrong. So what's the right
answer?

Decorators like `@wraps` or `@classmethod` change the behaviour of a
callable, and do so by returning a new callable. Other decorators, like
`@property` accept a callable, but leave something completely different in
their place.

This flexibility isn’t just there for decorators that come with Python. You
can use this flexibility in your own decorators, and it offers the potential
for you to build interesting and unexpected new behaviour.

In this talk, we’ll look at why Python has decorators, and how you can build
them.

We’ll look at how some more popular decorators in Python do what they do.
Finally, we’ll look at how to harness decorators to do unexpected and
potentially horrible things with Python.

After this talk, you’ll know how decorators work, how to write your own
decorators, and how decorators enable you to do extremely weird things in
Python. Most importantly, you’ll understand how to use decorators
responsibly, and definitely not to confuse your friends and colleagues.
