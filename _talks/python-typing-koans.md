---
duration: 10
presentation_url:
room: Online
slot: 2021-10-01 12:50:00-04:00
speakers:
- R.Kracekumar
title: "Python Typing Koans"
type: talk
video_url:
---
Gradual typing was introduced in Python 3.5 standard library. Since then a
lot of standard library changes, new PEPs, third-party libraries added
support to type-hints. The flask 2.0 supports type-hints, SQLAlchemy,
Django(through the third-party stubs ) and a lot more of the libraries
support gradual typing.

The resources to learn gradual typing are minimal and available in form of
talks. I have built a self-exploratory way to learn Python typing using the
TDD/koans concept, [Python Typing
Koans](https://github.com/kracekumar/python-typing-koans).

The materials has koans for learn typing the pure python code, Django code,
Django Rest Framework.

[Here is a small demo video](https://asciinema.org/a/419119)

In this 10 minutes talk, I'll show how to use the tool to learn gradual
typing with hand-picked examples from a wide range of topics by
adding/modifying the existing Python code and type-annotation.

Note: Gradual typing, type-hints, optional static typing all refers to the
same concept.

Links:

- Repo: [https://github.com/kracekumar/python-typing-koans](https://github.com/kracekumar/python-typing-koans)
- Demo Video: [https://asciinema.org/a/419119](https://asciinema.org/a/419119)
