---
duration: 25
presentation_url:
room: Online
slot: 2021-10-02 12:45:00-04:00
speakers:
- Anastasiia Tymoshchuk
title: "Continuous Documentation for your code"
type: talk
video_url:
---
Do you document your code? Do you think it is important?

Imagine that you need to get back to your code in 6 month after you wrote
it, there is always a big possibility that you will have to spend some time
to find out how this code works. Or if someone else wrote some code, which
is already in production and your task is to fix a bug in it and there is no
documentation and no one actually knows what this code does.

There are more benefits of implementing continuous documentation for the
code:

- easy to onboard new team members,
- easy to share knowledge,
- if this code is open source - easy to start contributing,
- easy to see purpose and motivation of each piece of code,
- easy to keep versioning for each new release of the code.


It this talk I will show the difference between documentation types and will
show a demo in the end of the talk.
