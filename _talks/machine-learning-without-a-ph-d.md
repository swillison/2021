---
duration: 25
presentation_url:
room: Online
slot: 2021-10-02 10:00:00-04:00
speakers:
- Aaron Ma
title: "Machine Learning without a Ph.D"
type: talk
video_url:
---
Demand for Machine Learning (ML) engineers has skyrocketed in recent years,
though are not enough engineers to fulfill that demand. In this talk, we'll
learn the core concepts of ML, then a deep dive into the fascinating world
of neural networks. By the end of this talk, you'll learn the tools you need so you
can be one of the highly specialized ML practitioners that FAANG companies
are looking for. Ready to explore the magical world of ML and master it
WITHOUT a Ph.D.? Let's get started.
