---
duration: 25
presentation_url:
room: Online
slot: 2021-10-02 11:00:00-04:00
speakers:
- Vinicius Gubiani Ferreira
title: "Getting started with Fuzz testing"
type: talk
video_url:
---
Is your project safe against unforeseen scenarios? Are you sure? How about
if we take it for a ride then? There is almost no software completely error
free. And catch all of the possible errors is hard, not to say almost
impossible, but what if we use tools that help us bring possible issues to
our attention? Those that might cause security issues, or bad user
experience.

In this presentation I'll discuss how to get started with fuzz testing: The
art of hitting software with invalid, unexpected, or random data and seeing
how it reacts. I'll present valid scenarios for fixing the bugs found, some
of the existing tools, and how to integrate fuzz testing with our existing
tests.
