---
duration: 25
presentation_url:
room: Online
slot: 2021-10-01 14:25:00-04:00
speakers:
- Mark Smith
title: "Stupid Things I\u0027ve Done with Python"
type: talk
video_url:
---
On every computer I've had for the past 20 years, I've created a folder
called "stupid python tricks". It's where I put code that should never see
the light of day. Code that abuses advanced features like decorators,
metaclasses, and dynamic typing to do terrible things. Code I'm going to
teach you.

I have a [GitHub repository](https://github.com/judy2k/stupid-python-tricks)
that contains a compilation of all the awful code I've written over the
years, abusing features like metaclasses, decorators, various __dunder__
methods, and the mutable nature of Python's underlying data structures.

It's a funny way to see how Python works under the hood, but it's also
educational.

Really!

Each of the tricks is entertainingly terrible. But they're also mostly self-
contained ways to see what happens when you override certain behaviours of
the Python language. Anyone with an intermediate level of Python
understanding should be able to follow along and learn something.
