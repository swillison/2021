---
duration: 25
presentation_url:
room: Online
slot: 2021-10-01 14:00:00-04:00
speakers:
- Miroslav Šedivý
title: "There Are Python 2 Relics in Your Code"
type: talk
video_url:
---
Should we return to Python 2? No? Then why don't you get rid of those Python
2 relics from your code?

Migration to Python 3 is over, but that's not the end of the journey.
Although your code runs with the currently supported Python 3.6 to 3.9 (and
upcoming 3.10), there may be some pieces of code that look obvious to you,
but may surprise younger developers who have never seen Python 2 code.

Earlier this year, I helped dozens of Python projects on GitHub to get rid
of those Python 2 relics. I'll show you a few recipes beyond the automatic
tools, how to make your code modern and prepared for future updates.

And no, we should not return to Python 2. We should get rid of it
completely.
