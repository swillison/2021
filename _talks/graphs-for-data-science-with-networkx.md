---
duration: 25
presentation_url:
room: Online
slot: 2021-10-01 11:30:00-04:00
speakers:
- Bruno Gonçalves
title: "Graphs for Data Science with NetworkX"
type: talk
video_url:
---
Graphs are simple concepts: a set of individual Nodes (components) connected
by Edges (relationships). In this very simplicity lies their power. They can
describe the structure of our friendships, connections between airports, the
spread of diseases from person to person, the relation of one concept to
another, how species interact in an ecosystem or how computers communicate
to form the World Wide Web.

Networks are the fundamental language of our increasingly complex world and
the key to successfully understand it. By exploring in detail the way graphs
can be used to explore, describe, analyze and understand empirical datasets
we will put you at the forefront of this growing field.

In this lecture we will use Python's NetworkX to build our understanding of
network representations and algorithms by exploring real world network
datasets such as the airline transportation network, the bitcoin transaction
network, the road network of open street map, etc. Through this practical
approach attendees will better understand the fundamental ideas and concepts
that lie at the base of  our increasingly complex world and take the first
steps towards being at the forefront of this growing field.
