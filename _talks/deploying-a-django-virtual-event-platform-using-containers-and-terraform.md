---
duration: 25
presentation_url:
room: Online
slot: 2021-10-02 12:00:00-04:00
speakers:
- Calvin Hendryx-Parker
title: "Deploying a Django Virtual Event Platform Using Containers and Terraform"
type: talk
video_url:
---
Learn to leverage cloud native tools and launch a scalable Python and Django
application into the Cloud with Fargate. We’ll dive in with how to getting
up and running fast, but leaving the overhead of managing virtual machines
and Kubernetes behind. Create and store the application Docker images in a
container repository and without touching the AWS console we can create
fully Infrastructure as Code automated deployments via CodePipeline into
Fargate containers and S3 buckets. Deliver the React application via
CloudFront and S3 for full global scalability. Leave the legacy deployments
behind and forge bravely into the new world of Cloud Native applications.
