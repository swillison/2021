---
duration: 10
presentation_url:
room: Online
slot: 2021-10-01 13:30:00-04:00
speakers:
- Dane Hillard
title: "Keeping code safe and modern with semantic searches"
type: talk
video_url:
---
Linting helps us avoid common mistakes and use our team's preferred style
and syntax. Deprecation warnings help us know that changes may be coming
soon and offer suggestions about how to migrate.

Existing linting tools and deprecation systems aren't always customizable or
extensible to organization-specific checks. Some constructs are also
difficult to search for with regular expressions, which is a common
configuration offering.

The time leading up to and during a migration often becomes a pain, leading
to all-at-once changes that slow teams down. Can we do better?
