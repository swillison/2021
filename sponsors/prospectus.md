---
title: Sponsorship Prospectus
---

## So you want to sponsor {{ site.data.event.name }}...

You're awesome!

{{ site.data.event.name }} is a brand new type of PyGotham, and we're excited
to have you.

## Quick Facts

- PyGotham's in-person conferences sell out every year. This year, PyGotham is
  fully online and free to attend, and we expect our largest audience ever in
  2021.

- The schedule contains two full days of talks, breakout rooms, and other
  uniquely remote content. Talks will cover topics including core Python, data
  science, web development, and more. For an idea of what to expect, take a look
  previous years' schedules:
  [2020](https://2020.pygotham.tv/talks/schedule/),
  [2019](https://2019.pygotham.org/talks/schedule/),
  [2018](https://2018.pygotham.org/talks/schedule/),
  [2017](https://2017.pygotham.org/talks/schedule/),
  [2016](https://2016.pygotham.org/talks/schedule/),
  [2015](https://2015.pygotham.org/talks/schedule/),
  [2014](https://2014.pygotham.org/talks/schedule.html).

- PyGotham strives to be accessible to everyone. Since 2019, every talk was
  accompanied by live captioning and ASL interpretation for the first time, and
  this year's talks will too.

PyGotham sponsorship tiers are in the works and will be published soon. If you
have specific sponsorship goals and would like to start that conversation early,
you can reach us at [sponsors@pygotham.org](mailto:sponsors@pygotham.org).

{% comment %}

In order to keep the conference free for everyone, we need your help.
Several different levels of sponsorship are available. If you have any
questions, you can contact us directly.

Once you're ready, go ahead and send an email to
[sponsors@pygotham.org](mailto:sponsors@pygotham.org).

## Corporate Sponsorship Levels

| Benefit                                   | Platinum  | Gold       | Silver   |
|                                           | $5,000    | $2,500     | $1,000   |
| :---------------------------------------: | :-------: | :--------: | :------: |
| Limited to                                | 4         | ∞          | ∞        |
| Logo on video bumper                      | ✅        |            |          |
| Virtual conference booth                  | ✅        | ✅         |          |
| Video ads between conference sessions     | ✅        | ✅         |          |
| Logo and text in conference media         | ✅        | ✅         | ✅       |
| Announcements in social media             | ✅        | ✅         | ✅       |
{: .table.table-striped.table-bordered }

## FAQ

### Does PyGotham offer Community and Open Source sponsorships?
Absolutely! Gold tier and silver tier equivalent sponsorships are available. If
your organization or project is part of the Python community (or would like to
be), reach out to [sponsors@pygotham.org](mailto:sponsors@pygotham.org).

### What are sponsor video ads?
To stay true to the network television experience, PyGotham TV will air ads
between conference talks. Examples include product demos, testimonials, and even
infomercials; fun and creativity are encouraged. Note: all ads must be approved
by conference staff prior to the event.

{% endcomment %}
